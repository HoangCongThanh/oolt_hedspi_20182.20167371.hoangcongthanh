import java.util.Scanner;
public class Displaydays {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input month: ");
		int month = keyboard.nextInt();
		System.out.println("Input year: ");
		int year = keyboard.nextInt();
		int days;
		int checkplus = year%4;
		
		if(month<=7&&month%2!=0) {
			days = 31;
		}
		else if(month<=7&&month%2==0) {
			if(month==2&&checkplus==0)
				days =29;
			else if(month==2&&checkplus!=0)
				days =28;
			else
				days =30;
		}
		else if(month>=7&&month%2==0)
			days =31;
		else
			days =30;
		System.out.println("The days in that month and year are: "+days);
	}
}
