import java.util.Arrays;

public class Sort {
	public static void main(String[] args) {
		int[] array = {1789,2035,1899,1456,2013};
		double sum=0,average=0;
		int i;
		for(i=0;i<array.length;i++)
			sum+=array[i];
		average = sum/array.length;
		Arrays.sort(array);
		System.out.println("Sum of the array: "+sum);
		System.out.println("Average of the array: "+average);
		System.out.println("The array after sorting:");
		for(i=0;i<array.length;i++)
			System.out.print(" "+array[i]);
	}
}
