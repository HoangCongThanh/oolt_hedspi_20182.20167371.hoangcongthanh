public class Aims{
    public static void main(String [] args) {
       DigitalVideoDisc dvd1 = new DigitalVideoDisc("steins;gate","time","mrA",10,(float)10.0);
       DigitalVideoDisc dvd2 = new DigitalVideoDisc("angel beats","fantasy","mrB",11,(float)11.0));
       DigitalVideoDisc dvd3 = new DigitalVideoDisc("kimi no nawa","time","mrC",12,(float)12.0); 
       Order list = new Order(); 
     
       DigitalVideoDisc [] dvdList = {dvd1,dvd2,dvd3};
       
       list.addDVD(dvd1,dvd2);
       list.print();
       
       Order list2 = new Order();
       list2.addDVD(dvd3);
       list2.print();
    }
}