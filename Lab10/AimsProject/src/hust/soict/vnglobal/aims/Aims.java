package hust.soict.vnglobal.aims;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.vnglobal.aims.media.Book;
import hust.soict.vnglobal.aims.media.CompactDisc;
import hust.soict.vnglobal.aims.media.DigitalVideoDisc;
import hust.soict.vnglobal.aims.media.Track;
import hust.soict.vnglobal.aims.memorydaemon.MemoryDaemon;
import hust.soict.vnglobal.aims.order.Order;

public class Aims{
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("-------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add items to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		
		System.out.println("0. Exit");
		System.out.println("-------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");

	}
    public static void main(String [] args) throws PlayerException {
    	Scanner keyboard = new Scanner(System.in);
    	Order list = new Order();
    	Thread thread = new Thread(new MemoryDaemon());
    	int length;
    	String director,artist;
    	int ch,choice,numb,num;
    	
    	thread.setDaemon(true);
    	thread.start();
    	
    	do{	
    		showMenu();
    		ch = keyboard.nextInt();
    		switch(ch) {
	    		case 1:
	    			System.out.println("Order is created");break;
	    			
	    		case 2:
	    			System.out.println("Input the number of items you want to add: ");
	    			numb=keyboard.nextInt();
	    			keyboard.nextLine();
	    			
	    			for(int i = 0; i<numb; i++) {
		    			String[] str= new String[5];
		    			
	    				System.out.println("Input the title of the item "+(i+1));
	    				str[0] = keyboard.nextLine();
	    				System.out.println("Input the category of the item "+(i+1));
						str[1]=keyboard.nextLine();
						System.out.println("Input the cost of the item "+(i+1));
						str[2]=keyboard.nextLine();
						
							System.out.println("Input 1 for book, 2 for dvd, 3 for compactdisc:");
							choice = keyboard.nextInt();
							switch(choice) {
							case 1:			
								System.out.println("Input the number of authors:");
								int number = keyboard.nextInt();
								keyboard.nextLine();
								List<String>tempstr= new ArrayList<String>();

								for(int j=0;j<number;j++) {
									System.out.println("Input authors "+(j+1));
									tempstr.add(keyboard.nextLine());
								}
								Book temp = new Book(str[0],str[1],Float.parseFloat(str[2]),tempstr);
								list.addMedia(temp);
								break;
							case 2:
								System.out.println("Input the length of the dvd:");
								length = keyboard.nextInt();
								keyboard.nextLine();
								System.out.println("Input the director of the dvd:");
								director = keyboard.nextLine();
								DigitalVideoDisc temp2 = new DigitalVideoDisc(str[0],str[1],director,length,Integer.parseInt(str[2]));
								list.addMedia(temp2);
								break;
								
							case 3:
								System.out.println("Input the length of the compact disc:");
								length = keyboard.nextInt();
								keyboard.nextLine();
								System.out.println("Input the artist of the compact disc:");
								artist = keyboard.nextLine();
								CompactDisc temp3 = new CompactDisc(str[0],str[1],Float.parseFloat(str[2]),length,artist);
								System.out.println("Please input number of tracks in compact disc:");
								num = keyboard.nextInt();
								keyboard.nextLine();
								for(int j=0;j<num;j++) {
									System.out.println("Input the title of track "+(j+1));
									String title = keyboard.nextLine();
									System.out.println("Input the length of track "+(j+1));
									length = keyboard.nextInt();
									keyboard.nextLine();
									Track temp4 = new Track(title,length);
									temp3.addTrack(temp4);
									temp4.play();
								}
								list.addMedia(temp3);

								break;
							default:System.out.println("Wrong input");break;
							}
	    			}
	    			break;
	    			
	    		case 3:
	    			System.out.println("Input the id of the item you want to delete:");
	    			int id = keyboard.nextInt();
	    			keyboard.nextLine();
	    			list.delete(id);
	    			System.out.println("Deleting is done");
	    			break;
	    		case 4:list.print();break;
	    		case 0:break;
	    		default:System.out.println("Wrong input!");break;
    		}
    	}while(ch!=0);
    	
    	System.out.println("************Start Lab 8*************");
    	DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Matrix");
    	DigitalVideoDisc dvd2 = new DigitalVideoDisc("The Matrix: Reloaded");
    	DigitalVideoDisc dvd3 = new DigitalVideoDisc("The Matrix: The Revolution");

    	java.util.Collection collection = new java.util.ArrayList();
    	collection.add(dvd3);
    	collection.add(dvd1);
    	collection.add(dvd2);
    	
    	java.util.Iterator iterator = collection.iterator();
    	System.out.println("-------------------------------------");
    	System.out.println("The DVDs currently in the order are: ");
    	
    	while(iterator.hasNext()) {
    		System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
    	}
    	
    	java.util.Collections.sort((java.util.List)collection);
    	
    	iterator = collection.iterator();
    	
    	System.out.println("--------------------------------------");
    	System.out.println("The DVDs in sorted order are: ");
    	
    	while (iterator.hasNext()) {
    		System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
    	}
    	
    	System.out.println("---------------------------------------");
      }
}