package hust.soict.vnglobal.aims.media;

import hust.soict.vnglobal.aims.PlayerException;

public interface Playable {
	public void play() throws PlayerException;
}
