package hust.soict.vnglobal.aims.media;

import hust.soict.vnglobal.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable,Comparable{
    
	public void play() throws PlayerException{
		if(this.getLength()<=0) {
			System.err.println("ERROR: TRACK length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: "+this.getTitle());
    	System.out.println("DVD length: "+this.getLength());
	}
	
    public DigitalVideoDisc(String title){
    	super(title);
    }

    public DigitalVideoDisc(String title, String category){
        super(title,category);
    }

    public DigitalVideoDisc(String title, String category, String director){
        super(title,category);
        setDirector(director);
    }

    public DigitalVideoDisc(String title, String category, String director,int length, float cost){
    	super(title,category,cost);
        setDirector(director);
        setLength(length);
    }
    
    public boolean search (String title) {
    	int flag = 0;
    	int j,i;
    	String[] temp1 = title.split("\\s+");
    	String[] temp2 = this.getTitle().split("\\s+");
    	for( i=0;i<temp1.length;i++) {
    		for( j=0;j<temp2.length;j++)
    			if(temp1[i].equals(temp2[j]))
    				flag++;
    	}
    	if(flag==temp2.length)
    		return true;
    	return false;
    }

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		//o = new DigitalVideoDisc();
		DigitalVideoDisc temp = (DigitalVideoDisc) o;
		return this.getTitle().compareTo(temp.getTitle());
	}
}