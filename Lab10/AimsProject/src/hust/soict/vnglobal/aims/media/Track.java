package hust.soict.vnglobal.aims.media;

import hust.soict.vnglobal.aims.PlayerException;

public class Track implements Playable,Comparable{
	private String title;
	private int length;
	
	public Track(String title, int length) {
		setTitle(title);
		setLength(length);
	}
	public void play() throws PlayerException{
		if(this.getLength()<=0) {
			System.err.println("ERROR: TRACK length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: "+this.getTitle());
    	System.out.println("DVD length: "+this.getLength());
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public int compareTo(Object o) {
		Track temp = (Track) o;
		return this.getTitle().compareTo(temp.getTitle());
	}
}
