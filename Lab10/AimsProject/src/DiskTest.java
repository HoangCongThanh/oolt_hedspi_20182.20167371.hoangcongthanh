
import hust.soict.vnglobal.aims.media.DigitalVideoDisc;
import hust.soict.vnglobal.aims.order.Order;

public class DiskTest {
	public static void main(String [] args) {
	 DigitalVideoDisc dvd1 = new DigitalVideoDisc("steins;gate","time","mrA",10,(float)10.0);
     DigitalVideoDisc dvd2 = new DigitalVideoDisc("angel beats","fantasy","mrB",11,(float)11.0);
     //DigitalVideoDisc dvd3 = new DigitalVideoDisc("kimi no nawa","time","mrC",12,(float)12.0); 
     Order list = new Order(dvd1,dvd2);
     System.out.println(dvd2.search("beats       angel"));
     //System.out.println("LUCKY ITEM IS: "+list.getALuckyItem().getTitle());
     list.print();
  	}
}
