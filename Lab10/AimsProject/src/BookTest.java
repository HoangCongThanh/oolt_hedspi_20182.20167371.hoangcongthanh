import hust.soict.vnglobal.aims.media.Book;

public class BookTest {
	public static void main(String[] args) {
		
		Book book = new Book("Tham tu lung danh Conan tap 100","trinh tham");
		book.setContent("Bo truyen ve Shinichi Kudo : cau tham tu bi teo nho. Day la tap cuoi cua bo truyen. Cai ket la dieu ma cac fan deu dang mong doi doi voi so phan cua cau tham tu lung danh nay !");
		book.processContent();
		System.out.println(book.toString());
	}
}
