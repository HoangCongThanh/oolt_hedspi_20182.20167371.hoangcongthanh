import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Manager {
	
	public static void main(String[] args) {
		int flag;
		String temp;
		Date date = new Date();
		Student student = new Student();
		Scanner keyboard = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
		
		System.out.println("Input studentID: ");
		student.setStudentID(keyboard.nextInt());
		keyboard.nextLine();
		
		System.out.println("Input student name: ");
		student.setStudentName(keyboard.nextLine());
		
		do {
		System.out.println("Input student birdthday :");
		temp = keyboard.nextLine();
		try {
			flag = 1;
			date = sdf.parse(temp);
		}catch(Exception e) {
			System.out.println("Birthday format is wrong");
			flag = 0;
		}
		}while(flag==0);
		
		do {
			System.out.println("Input student GPA :");
			try {
			temp = keyboard.nextLine();
				flag = 1;
				int i = Integer.parseInt(temp);
				if(i<0||i>4) {
					System.out.println("Not a valid number (must be between 0 and 4)");
					flag = 0;
				}
			}catch(Exception e) {
				flag = 0;
				System.out.println("Wrong Input");
			}
			}while(flag==0);
	}

}
