package hust.soict.vnglobal.aims.Aims;
import hust.soict.vnglobal.aims.order.Order.Order;
import hust.soict.vngobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;

public class Aims{
    public static void main(String [] args) {
       DigitalVideoDisc dvd1 = new DigitalVideoDisc("steins;gate","time","mrA",10,(float)10.0);
       DigitalVideoDisc dvd2 = new DigitalVideoDisc("angel beats","fantasy","mrB",11,(float)11.0);
       DigitalVideoDisc dvd3 = new DigitalVideoDisc("kimi no nawa","time","mrC",12,(float)12.0); 
       DigitalVideoDisc [] dvdList = {dvd1,dvd2,dvd3};

       Order list = new Order(dvdList);
       list.print();
       
       Order list1 = new Order(dvd1,dvd2); 
       list1.print();
       
       Order list2 = new Order(dvd3);
       list2.print();
       
      }
}