package hust.soict.vngobal.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Book extends Media implements Comparable{

	private List<String> authors = new ArrayList<String>();
	private String content;
	List<String> contentTokens = new ArrayList<String>();
	Map<String,Integer> wordFrequency = new TreeMap<String,Integer>();
	
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title,category);
	}
	
	public Book(String title, String category, float cost, List<String> authors){
		super(title,category,cost);
		setAuthors(authors);
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	////////////////////////////////////////////////
	
	public void addAuthor(String authorName) {
		int flag=0;
		for(String temp : authors) {
			if(temp.equals(authorName)) {
				System.out.println("This author is already here!");
				flag++;
				break;
			}
		}
		if(flag==0) {
			authors.add(authorName);
			System.out.println("This author has been added");
		}
	}
	
	public void removeAuthor(String authorName) {
		int flag=0;
		for(String temp : authors) {
			if(temp.equals(authorName)) {
				flag++;
				authors.remove(authorName);
				System.out.println("This author has been removed!");
				break;
			}
		}
		if(flag==0)
			System.out.println("There is no author like that");
	}
	
	public int compareTo(Media obj) {
		return this.getTitle().compareTo(obj.getTitle());
	}

	@Override
	public int compareTo(Object o) {
		Book temp = (Book) o;
		return this.getTitle().compareTo(temp.getTitle());
	}
	
	public void processContent() {
		String[] temp = content.split("[\\p{Punct}\\s]+");
		for(int i=0; i<temp.length; i++) {
			contentTokens.add(temp[i]);
		}
		Collections.sort(contentTokens);
		Set<String> distinct = new HashSet<String>(contentTokens);
		for(String s: distinct) {
			wordFrequency.put(s,Collections.frequency(contentTokens, s));
		}
	}
	
	@Override
	public String toString() {
		String content="";
		int j=0;
		content+=("Book title: "+this.getTitle());
		
		content+=("\r\nCategory: "+this.getCategory());
		
		content+=("\r\nBook's tokens list: ");
		for(Map.Entry<String,Integer> entry : wordFrequency.entrySet()) {
			String key = entry.getKey();
			Integer value = entry.getValue();
			content+=(key+"-"+value+", ");
		}
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
}
