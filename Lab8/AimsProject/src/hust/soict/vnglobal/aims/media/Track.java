package hust.soict.vngobal.aims.media;

public class Track implements Playable, Comparable{
	private String title;
	private int length;
	
	public Track(String title, int length) {
		setTitle(title);
		setLength(length);
	}
	public void play() {
		System.out.println("Playing DVD: "+this.getTitle());
    	System.out.println("DVD length: "+this.getLength());
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public int compareTo(Object o) {
		Track temp = (Track) o;
		return this.getTitle().compareTo(temp.getTitle());
	}
}
