package hust.soict.vnglobal.aims.memorydaemon;
public class MemoryDaemon implements java.lang.Runnable{
	long memoryUsed = 0;
	public MemoryDaemon() {
		
	}

	@Override
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		
		while(true) {
			used = rt.totalMemory() - rt.freeMemory();
			if(used != memoryUsed) {
				System.out.println("\t Memory used= "+used);
				memoryUsed = used;
			}
		}		
	}
}
