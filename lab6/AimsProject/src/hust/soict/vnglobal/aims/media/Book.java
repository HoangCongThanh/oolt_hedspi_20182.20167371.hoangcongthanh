package hust.soict.vnglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {

	private List<String> authors = new ArrayList<String>();
	
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title,category);
	}
	
	public Book(String title, String category, float cost, List<String> authors){
		super(title,category,cost);
		setAuthors(authors);
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	////////////////////////////////////////////////
	
	public void addAuthor(String authorName) {
		int flag=0;
		for(String temp : authors) {
			if(temp.equals(authorName)) {
				System.out.println("This author is already here!");
				flag++;
				break;
			}
		}
		if(flag==0) {
			authors.add(authorName);
			System.out.println("This author has been added");
		}
	}
	
	public void removeAuthor(String authorName) {
		int flag=0;
		for(String temp : authors) {
			if(temp.equals(authorName)) {
				flag++;
				authors.remove(authorName);
				System.out.println("This author has been removed!");
				break;
			}
		}
		if(flag==0)
			System.out.println("There is no author like that");
	}
}
