package hust.soict.vnglobal.aims.media;
public class DigitalVideoDisc extends Media{
   
    private String director;
    private int length;

    public void setDirector(String director){
        this.director=director;
    }
    public String getDirector(){
        return director;
    }

    public void setLength(int length){
        this.length=length;
    }

    public int getLength(){
        return length;
    }

    public DigitalVideoDisc(String title){
    	super(title);
    }

    public DigitalVideoDisc(String title, String category){
        super(title,category);
    }

    public DigitalVideoDisc(String title, String category, String director){
        super(title,category);
        setDirector(director);
    }

    public DigitalVideoDisc(String title, String category, String director,int length, float cost){
    	super(title,category,cost);
        setDirector(director);
        setLength(length);
    }
    
    public boolean search (String title) {
    	int flag = 0;
    	int j,i;
    	String[] temp1 = title.split("\\s+");
    	String[] temp2 = this.getTitle().split("\\s+");
    	for( i=0;i<temp1.length;i++) {
    		for( j=0;j<temp2.length;j++)
    			if(temp1[i].equals(temp2[j]))
    				flag++;
    	}
    	if(flag==temp2.length)
    		return true;
    	return false;
    }
}