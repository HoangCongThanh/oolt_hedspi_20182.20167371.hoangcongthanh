import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class MyDate{
    private int day=0, month=0, year=0;

    public MyDate(){
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        this.day = localDate.getDayOfMonth();
        this.month = localDate.getMonthValue();
        this.year = localDate.getYear();
    }

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public MyDate(String date){
        takeElement(date);
    }
    public void takeElement(String date){
        String[] array = date.split("\\s+");
        
        this.setYear(Integer.parseInt(array[2]));
        switch(array[0]){
            case "January":this.month =1;break;
            case "Febuary":this.month =2;break;
            case "March":this.month =3;break;
            case "April":this.month =4;break;
            case "May":this.month =5;break;
            case "June":this.month =6;break;
            case "July":this.month =7;break;
            case "August":this.month =8;break;
            case "September":this.month =9;break;
            case "October":this.month =10;break;
            case "November":this.month =11;break;
            case "December":this.month =12;break;
            default: this.month =0;break;
        }
        this.setDay(Integer.parseInt(array[1].replaceAll("[a-z]","")));
    }

    public void setDay(int day){
        if(this.month<=7&&this.month%2!=0) {
            if(day>0&&day<32)
                this.day=day;
		}
		else if(this.month<=7&&this.month%2==0) {
			if(this.month==2&&this.year%4==0)
                if(day==29)
                    this.day = day;
			else if(this.month==2&&this.year%4!=0)
                if(day==28)
                    this.day = day;
			else
                if(day>0&&day<31)
                    this.day = day;
		}
		else if(this.month>=7&&this.month%2==0)
            if(day>0&&day<32)
                this.day = day;
		else
            if(day>0&&day<31)
                this.day = day;
    }

    public void setMonth(int month){
        if(month>0&&month<13)
            this.month = month;
    }

    public void setYear(int year){
        if(year>=0)
            this.year = year;
    }

    public int getDay(){
        return day;
    }

    public int getMonth(){
        return month;
    }

    public int getYear(){
        return year;
    }

    public void accept(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input a date: ");
        String temp = keyboard.nextLine();
        this.takeElement(temp);
    }

    public void print(){
        System.out.println("MyDate is: "+this.year+"/"+this.month+"/"+this.day);
    }
}