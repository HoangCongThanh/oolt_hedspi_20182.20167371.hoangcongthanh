package hust.soict.vnglobal.aims.order;

import java.time.LocalDateTime;
import java.util.ArrayList;
//import java.util.Random;

import hust.soict.vnglobal.aims.media.Media;

public class Order{

    public static final int MAX_NUMBER_ORDERED = 10;
    private float sum = 0;
    private LocalDateTime dateOrdered = LocalDateTime.now();
    private int numofOrder;
    //private int luck=0;
    //Media luckitem = new Media();
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    
    public LocalDateTime getDate() {
    	return dateOrdered;
    }
    
    public void setDate(int year, int month, int day, int hour, int minute, int second) {
    	dateOrdered.withYear(year);
    	dateOrdered.withDayOfMonth(day);
    	dateOrdered.withMonth(month);
    	dateOrdered.withHour(hour);
    	dateOrdered.withMinute(minute);
    	dateOrdered.withSecond(second);
    }
    
    public Order() {
    	
    }
    
    public Order(Media obj){	//Add 1dvd
    	numofOrder=addnbOrders();
    	if(numofOrder!=-1) {
        if(itemsOrdered.size()+1<=MAX_NUMBER_ORDERED){
            itemsOrdered.add(obj);
            sum+=obj.getCost();
        }
        else
            System.out.println("List is full!!!");
    	}
    }
    
    public Order(Media obj1, Media obj2) {
    	numofOrder=addnbOrders();
    	if(numofOrder!=-1) {
    	addMedia(obj1);
    	sum+=obj1.getCost();
    	addMedia(obj2);
    	sum+=obj2.getCost();
    	}
    }
    
    public Order(Media [] objList) {
    	numofOrder=addnbOrders();
    	if(numofOrder!=-1)
    		for(int i=0;i<objList.length;i++) {
    			addMedia(objList[i]);
    			sum+=objList[i].getCost();
    		}
    }
    public float totalCost(){
        return sum;
    }
    
    public static class nbOrders{
    	public static final int MAX_LIMITTED_ORDER = 5;
    	private static int orders = 0;
    }
    
    public int addnbOrders(){
    	if(nbOrders.orders+1<=nbOrders.MAX_LIMITTED_ORDER) {
    		nbOrders.orders++;
    		return nbOrders.orders;
    	}
    	else {
    		System.out.println("************ WARNING ************");
    		System.out.println("Max limitted order is 5 !!!");
    	}
    	return -1;
    	
    }
    
    /*public Media getALuckyItem() {
    	Random rd = new Random();
    	Media foo = new Media();
    	int num = rd.nextInt(itemsOrdered.size());
    	int count = 0;
    	for(Media temp : itemsOrdered) {
        	if(count == num) {
        		this.sum-=temp.getCost();
        		luck = 1;
        		luckitem = temp;
        		return temp;
        	}
        	else
        		count++;
    		}
    	return foo;
    }*/
    
    
    public void addMedia(Media obj) {
    	if(itemsOrdered.size()+1>MAX_NUMBER_ORDERED) {
    		System.out.println("List is full !");
    	}
    	else {
    		itemsOrdered.add(obj);
    		sum+=obj.getCost();
    	}
    }
    
    public void removeMedia(Media obj) {
    	int flag = 0;
    	for(int i=0; i<itemsOrdered.size(); i++) {
    		if(itemsOrdered.get(i)==obj) {
    			flag++;
    			sum-=obj.getCost();
    			itemsOrdered.remove(i);
    			System.out.println("The ordered item "+"\""+obj.getTitle()+"\" has been removed !");
    		}
    	}
    	if(flag==0)
    		System.out.println("The ordered item "+"\""+obj.getTitle()+"\" doesn't exist!");
    }
    /////////////////////////////////////////////////////////
    public void delete(int id) {
    	int flag=0;
    	for(int i=0;i<itemsOrdered.size();i++) {
    		if(id==itemsOrdered.get(i).getId()) {
    			removeMedia(itemsOrdered.get(i));
    			flag++;
    			break;
    		}
    	}
    	if(flag==0)
    		System.out.println("There is no item that has id like that!");
    }
    public void print() {
    	if(numofOrder!=-1) {
	    	System.out.println("***************************Order(#"+this.numofOrder+")****************************");
	    	System.out.println("Date: "+this.getDate().getYear()+"/"+this.getDate().getMonthValue()+"/"+this.getDate().getDayOfMonth()+
	    			" "+this.getDate().getHour()+":"+this.getDate().getMinute()+":"+this.getDate().getSecond());
	    	System.out.println("Ordered Items:");
	    	for(Media temp : itemsOrdered) {
	    		/*if(luck ==1 && temp == luckitem) {
	    			System.out.println(temp.getTitle()+" - "+temp.getCategory()+" - "+": "+"$"+"0 (lucky free item just 4 YOU <3)");
	    		}
	    		else*/
	    			System.out.println(temp.getTitle()+" - "+temp.getCategory()+" - "+": "+"$"+temp.getCost());
	    	}
	    	System.out.println("Total cost: "+"$"+this.totalCost());
    	}
    }
}