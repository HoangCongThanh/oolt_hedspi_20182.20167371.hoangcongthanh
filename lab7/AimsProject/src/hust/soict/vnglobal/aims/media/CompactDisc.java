package hust.soict.vnglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private int length;
	private ArrayList<Track> track = new ArrayList<Track>();
	
	public void play() {
		System.out.println("Playing "+this.getTitle()+"\'s tracks...........");
		for(int i=0;i<track.size();i++) {
			System.out.println("Playing DVD: "+track.get(i).getTitle());
	    	System.out.println("DVD length: "+track.get(i).getLength());
	    	System.out.println("");
		}
	}
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public CompactDisc(String title, String category, float cost, int length, String artist) {
		super(title,category,cost);
		this.length=length;
		this.artist=artist;
	}
	
	public void addTrack(Track track) {
		int flag=0;
		for(int i=0;i<this.track.size();i++) {
			if(track == this.track.get(i)) {
				System.out.println("This track already exists !");
				flag++;
				break;
			}	
		}
		if(flag==0) {
			this.track.add(track);
			length+=track.getLength();
			System.out.println("Adding completed");
		}
	}
	
	public void removeTrack(Track track) {
		int flag=0;
		for(int i=0;i<this.track.size();i++) {
			if(track == this.track.get(i)) {
				length-=this.track.get(i).getLength();
				this.track.remove(i);
				System.out.println("Removing completed");
				flag++;
				break;
			}	
		}
		if(flag==0) {
			System.out.println("This track doesn't exist");
		}
	}
	
	public int getLength() {
		return length;
	}
}
