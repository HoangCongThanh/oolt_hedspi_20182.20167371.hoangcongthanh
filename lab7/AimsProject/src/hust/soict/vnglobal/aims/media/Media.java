package hust.soict.vnglobal.aims.media;

public abstract class Media {
	private String title;
	private String category;
	private float cost;
	private int idofItem;
	
	public int getId() {
		return idofItem;
	}
	
	public Media() {
	}
	
	public Media(String title) {
		idofItem=addidItem();
		setTitle(title);
	}
	
	public Media(String title, String category) {
		idofItem=addidItem();
		setTitle(title);
		setCategory(category);
	}
	
	public Media(String title, String category, float cost) {
		idofItem=addidItem();
		setTitle(title);
		setCategory(category);
		setCost(cost);
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
	public static class idItem{
		private static int id = 1;
	}
	
	public int addidItem() {
		return idItem.id++;
	}

}
