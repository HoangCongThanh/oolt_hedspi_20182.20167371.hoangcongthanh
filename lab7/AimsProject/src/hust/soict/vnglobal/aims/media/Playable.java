package hust.soict.vnglobal.aims.media;

public interface Playable {
	public void play();
}
