package hust.soict.vnglobal.aims.order.Order;
import java.util.Set;
import hust.soict.vngobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Random;

public class Order{

    public static final int MAX_NUMBER_ORDERED = 10;
    Set<DigitalVideoDisc> itemOrdered = new HashSet<DigitalVideoDisc>();
    private int qtyOrdered= 0;
    private float sum = 0;
    private LocalDateTime dateOrdered = LocalDateTime.now();
    private int numofOrder, luck=0;
    DigitalVideoDisc luckitem = new DigitalVideoDisc("NULL");
    public LocalDateTime getDate() {
    	return dateOrdered;
    }
    
    public void setDate(int year, int month, int day, int hour, int minute, int second) {
    	dateOrdered.withYear(year);
    	dateOrdered.withDayOfMonth(day);
    	dateOrdered.withMonth(month);
    	dateOrdered.withHour(hour);
    	dateOrdered.withMinute(minute);
    	dateOrdered.withSecond(second);
    }
    
    public void setQty(int qtyOrdered){
        this.qtyOrdered+=qtyOrdered;
    }
   
    public int getQty(){
        return qtyOrdered;
    }
    //////////////////////

    public Order(DigitalVideoDisc dvd){	//Add 1dvd
    	numofOrder=addnbOrders();
    	if(numofOrder!=-1) {
        if(getQty()+1<=MAX_NUMBER_ORDERED){
            itemOrdered.add(dvd);
            this.setQty(1);
        }
        else
            System.out.println("List is full!!!");
    	}
    }
    
    public Order(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
    	numofOrder=addnbOrders();
    	if(numofOrder!=-1) {
    	DigitalVideoDisc [] dvdList = {dvd1,dvd2};
    	addDVD(dvdList);
    	}
    }
    
    public Order(DigitalVideoDisc [] dvdList) {
    	numofOrder=addnbOrders();
    	if(numofOrder!=-1)
    		addDVD(dvdList);
    }
    public void addDVD(DigitalVideoDisc [] dvdList) {	//Add dvdList
    	int checkpoint=0;
    	if(getQty()+dvdList.length>MAX_NUMBER_ORDERED) {
	    	for(int i=0;i<MAX_NUMBER_ORDERED-getQty();i++) {
	    		itemOrdered.add(dvdList[i]);
	    		checkpoint++;
	    	}	
	    	this.setQty(MAX_NUMBER_ORDERED);

	    	System.out.println("List is full !!!");
	    	System.out.println("List of left dvds: ");
	    	for(int i=checkpoint;i<dvdList.length;i++) {
	    		System.out.println(dvdList[i].getTitle());
	    	}
    	}
    	else {
    		this.setQty(dvdList.length);
    		for(int i=0;i<dvdList.length;i++)
    			itemOrdered.add(dvdList[i]);
    	}
    }

    public void removeDVD(DigitalVideoDisc dvd){	//Remove 1dvd
        boolean isremoved = itemOrdered.remove(dvd);
        System.out.println("The item is removed: "+isremoved);
        this.setQty(-1);
    }

    public float totalCost(){
        for(DigitalVideoDisc temp : itemOrdered)
            sum+=temp.getCost();
        return sum;
    }
    
    public static class nbOrders{
    	public static final int MAX_LIMITTED_ORDER = 2;
    	private static int orders = 0;
    }
    
    public int addnbOrders(){
    	if(nbOrders.orders+1<=nbOrders.MAX_LIMITTED_ORDER) {
    		nbOrders.orders++;
    		return nbOrders.orders;
    	}
    	else {
    		System.out.println("************ WARNING ************");
    		System.out.println("Max limitted order is 5 !!!");
    	}
    	return -1;
    	
    }
    
    public DigitalVideoDisc getALuckyItem() {
    	Random rd = new Random();
    	DigitalVideoDisc foo = new DigitalVideoDisc("NULL");
    	int num = rd.nextInt(itemOrdered.size());
    	int count = 0;
    	for(DigitalVideoDisc temp : itemOrdered) {
        	if(count == num) {
        		this.sum-=temp.getCost();
        		luck = 1;
        		luckitem = temp;
        		return temp;
        	}
        	else
        		count++;
    		}
    	return foo;
    }
    /////////////////////////////////////////////////////////
    
    public void print() {
    	if(numofOrder!=-1) {
	    	System.out.println("***************************Order(#"+this.numofOrder+")****************************");
	    	System.out.println("Date: "+this.getDate().getYear()+"/"+this.getDate().getMonthValue()+"/"+this.getDate().getDayOfMonth()+
	    			" "+this.getDate().getHour()+":"+this.getDate().getMinute()+":"+this.getDate().getSecond());
	    	System.out.println("Ordered Items:");
	    	for(DigitalVideoDisc temp : itemOrdered) {
	    		if(luck ==1 && temp == luckitem) {
	    			System.out.println(temp.getTitle()+" - "+temp.getCategory()+" - "+temp.getDirector()+" - "+temp.getLength()+": "+"$"+"0 (lucky free item just 4 YOU <3)");
	    		}
	    		else
	    			System.out.println(temp.getTitle()+" - "+temp.getCategory()+" - "+temp.getDirector()+" - "+temp.getLength()+": "+"$"+temp.getCost());
	    	}
	    	System.out.println("Total cost: "+"$"+this.totalCost());
    	}
    }
}