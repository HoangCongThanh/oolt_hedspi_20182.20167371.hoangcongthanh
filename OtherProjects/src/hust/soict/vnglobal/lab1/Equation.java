package hust.soict.vnglobal.lab1;

import javax.swing.JOptionPane;

public class Equation{
    public static void main(String[] args){
        int choice;
        double num1,num2,num3,num4,num5,num6,ans1,ans2,del;
        JOptionPane.showMessageDialog(null, "0. Exit\n1. 1st degree equation(1 var)\n2. 1st degree equation(2 vars)\n3. 2nd degree equation(1 var)", "Help", JOptionPane.INFORMATION_MESSAGE);
        do{
            choice = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input your choice: ","Input your choice",JOptionPane.INFORMATION_MESSAGE));
            
            switch(choice){
                case 1:
                num1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 1st parameter: ","Input the 1st parameter",JOptionPane.INFORMATION_MESSAGE));
                num2 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 2nd parameter: ","Input the 2nd parameter",JOptionPane.INFORMATION_MESSAGE));

                if(num1!=0){
                    ans1 = -num2/num1;
                    JOptionPane.showMessageDialog(null,"The answer is: "+ans1,"The answer for 1st degree equation(1 var)", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    if(num2==0)
                        JOptionPane.showMessageDialog(null,"This equation has infinite answer","The answer for 1st degree equation(1 var)",JOptionPane.INFORMATION_MESSAGE);
                    else
                        JOptionPane.showMessageDialog(null,"This equation has no answer","The answer for 1st degree equation(1 var)",JOptionPane.INFORMATION_MESSAGE);
                }
                break;
                
                case 2:

                num1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 1st parameter: ","Input the 1st parameter",JOptionPane.INFORMATION_MESSAGE));
                num2 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 2nd parameter: ","Input the 2nd parameter",JOptionPane.INFORMATION_MESSAGE));
                num3 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 3rd parameter: ","Input the 3rd parameter",JOptionPane.INFORMATION_MESSAGE));
                num4 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 4th parameter: ","Input the 4th parameter",JOptionPane.INFORMATION_MESSAGE));
                num5 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 5th parameter: ","Input the 5th parameter",JOptionPane.INFORMATION_MESSAGE));
                num6 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 6th parameter: ","Input the 6th parameter",JOptionPane.INFORMATION_MESSAGE));

                if(num1*num5-num2*num4==0&&num3==num6)
                    JOptionPane.showMessageDialog(null,"This equation has infinite answer","The ansewr for 1st degree equation(2 vars)",JOptionPane.INFORMATION_MESSAGE);
                else if(num1*num5-num2*num4==0&&num3!=num6)
                    JOptionPane.showMessageDialog(null,"This equation has no answer","The answer for 1st degree equation(2 vars)",JOptionPane.INFORMATION_MESSAGE);
                else{
                    ans1 = (num3*num5-num2*num6)/(num1*num5-num2*num4);
                    ans2 = (num1*num6-num3*num4)/(num1*num5-num2*num4);
                    JOptionPane.showMessageDialog(null,"The 1st answer and 2nd answer are: "+ans1+" "+ans2,"The ansewr for 1st degree equation(2 vars)",JOptionPane.INFORMATION_MESSAGE);
                }
                break;

                case 3:
                num1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 1st parameter: ","Input the 1st parameter",JOptionPane.INFORMATION_MESSAGE));
                num2 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 2nd parameter: ","Input the 2nd parameter",JOptionPane.INFORMATION_MESSAGE));
                num3 = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input the 3rd parameter: ","Input the 3rd parameter",JOptionPane.INFORMATION_MESSAGE));
                del = num2*num2-4*num1*num3;
                if(del==0){
                    ans1 = -num2/2/num1;
                    JOptionPane.showMessageDialog(null,"The equation has only 1 answer: "+ans1,"The answer for 2nd degree equation(1 var)",JOptionPane.INFORMATION_MESSAGE);
                }
                else if(del>0){
                    ans1 = (-num2-Math.sqrt(del))/2/num1;
                    ans2 = (-num2+Math.sqrt(del))/2/num1;
                    JOptionPane.showMessageDialog(null,"The equation has only 2 answers: "+ans1+" and "+ans2,"The answer for 2nd degree equation(1 var)",JOptionPane.INFORMATION_MESSAGE);
                }
                else
                    JOptionPane.showMessageDialog(null,"The equation has no answer: ","The answer for 2nd degree equation(1 var)",JOptionPane.INFORMATION_MESSAGE);
                break;

                case 0:
                break;

                default:
                JOptionPane.showMessageDialog(null,"Wrong input detected!!!", "WARNING", JOptionPane.INFORMATION_MESSAGE);
                break;
            }
        }while(choice!=0);

        System.exit(0);

    }
}