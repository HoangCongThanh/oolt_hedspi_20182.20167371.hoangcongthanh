package hust.soict.vnglobal.lab1;

import javax.swing.JOptionPane;
public class Calculator{
    public static void main(String[] args){
        double num1, num2, sum, difference, product;
        boolean quotient=true;
        num1 = Double.parseDouble(JOptionPane.showInputDialog(null,"Please input the first number: "));
        num2 = Double.parseDouble(JOptionPane.showInputDialog(null,"Please input the second number: "));
        sum = num1 + num2;
        difference = num1 - num2;
        product = num1 * num2;
        
        if(num1%num2!=0)
            quotient = false;

        JOptionPane.showMessageDialog(null,sum,"Sum of 2 numbers is: ",JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null,difference,"Difference of 2 numbers is: ",JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null,product, "Product of 2 numbers is: ",JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null,quotient,"Quotient of 2 numbers is: ",JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}