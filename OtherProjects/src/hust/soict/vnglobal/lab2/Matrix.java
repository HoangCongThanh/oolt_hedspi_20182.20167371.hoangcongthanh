package hust.soict.vnglobal.lab2;

import java.util.Scanner;
public class Matrix {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input the row of 2 matrices: ");
		int row = keyboard.nextInt();
		System.out.println("Input the column of 2 matrices: ");
		int column = keyboard.nextInt();
		
		int[][] mtrA = new int[row][column];
		int[][] mtrB = new int[row][column];
		
		for(int i=0;i<row;i++) {
			for(int j=0;j<column;j++) {
				System.out.println("Input the value of element("+i+")("+j+") of matrix A");
				mtrA[i][j] = keyboard.nextInt();
				System.out.println("Input the value of element("+i+")("+j+") of matrix B");
				mtrB[i][j] = keyboard.nextInt();
				mtrA[i][j]+=mtrB[i][j];
			}
		}
		
		System.out.println("Answer: ");
		for(int i=0;i<row;i++) {
			System.out.print("| ");
			for(int j=0;j<column;j++) {
				System.out.print(mtrA[i][j]+" ");
			}
			System.out.println("|");
		}
	}
}
