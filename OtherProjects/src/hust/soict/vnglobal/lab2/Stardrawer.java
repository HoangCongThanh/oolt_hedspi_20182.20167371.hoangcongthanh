package hust.soict.vnglobal.lab2;

import java.util.Scanner;
public class Stardrawer {
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input your number: ");
		int num = keyboard.nextInt();
		int k = num, q=1;
		for(int i=1; i<=num; i++) {
			for(int j=1; j<=k; j++)
				System.out.print(" ");
			for(int l=1; l<=q; l++ )
				System.out.print("*");
			k--;
			q+=2;
			System.out.println("");
		}
	}
}
