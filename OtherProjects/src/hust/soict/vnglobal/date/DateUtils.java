package hust.soict.vnglobal.date;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
public class DateUtils {
	
	public static void compare(String d1, String d2) {
		Date date1 = new Date();
		Date date2 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
         date1 = sdf.parse(d1);
         date2 = sdf.parse(d2);
		}
		catch(Exception e){
			System.out.println("Wrong input !");
		}
		if (date1.compareTo(date2) > 0) {
            System.out.println("Date1 is after Date2");
        } else if (date1.compareTo(date2) < 0) {
            System.out.println("Date1 is before Date2");
        } else if (date1.compareTo(date2) == 0) {
            System.out.println("Date1 is equal to Date2");
        } else {
            System.out.println("How to get here?");
        }
	}
	
	public static void sort() {
		
	}
}
